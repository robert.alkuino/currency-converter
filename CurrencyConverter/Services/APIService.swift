//
//  APIService.swift
//  CurrencyConverter
//
//  Created by Robert John Alkuino on 10/7/22.
//

import Foundation
import RxSwift
import RxCocoa

//public struct APIResponse: Codable {
//
//    public let page: Int
//    public let totalResults: Int
//    public let totalPages: Int
//    public let results: [Movie]
//}

enum APIServiceError: Error {
    case invalidEndpoint
    case invalidResponse
    case decodeError
}

class APIService<T:Decodable> {
    
    
    private init() {}
    private static var urlSession:URLSession {
        return URLSession.shared
    }
    private static var baseURL:URL {
        return URL(string: "http://api.evp.lt")!
    }
    
    private static var jsonDecoder: JSONDecoder {
        let jsonDecoder = JSONDecoder()
        return jsonDecoder
    }
    
    enum Endpoint {
        case exchange(value:String,from:String,to:String)
        
        var description: String {
            switch self {
            case .exchange(let value,let from,let to):
                print("debug param","\(value)-\(from)/\(to)/latest")
                return "\(value)-\(from)/\(to)/latest"
            }
        }
    }
    
    private static func fetchResources(url: URL) -> Observable<T> {
        return Observable.create { observer in
            
            let task = urlSession.dataTask(with: url) { (data,response,error) in
                if let httpResponse = response as? HTTPURLResponse{
                    let statusCode = httpResponse.statusCode
                    do {
                        let _data = data ?? Data()
                        if (200...399).contains(statusCode) {
                          let objs = try self.jsonDecoder.decode(T.self, from: _data)
                          //MARK: observer onNext event
                            observer.onNext(objs)
                        } else {
                            observer.onError(APIServiceError.invalidResponse)
                        }
                    } catch {
                        observer.onError(APIServiceError.decodeError)
                    }
                    
                } else {
                    observer.onError(APIServiceError.invalidResponse)
                }
                //MARK: observer onCompleted event
                observer.onCompleted()
            }
            task.resume()
            
            //MARK: return our disposable
            return Disposables.create {
                task.cancel()
            }
        }
    }
    
    public static func latestExchangeConvertion(from endpoint: Endpoint) -> Observable<T> {
        let exchange = baseURL
            .appendingPathComponent("currency/commercial/exchange")
            .appendingPathComponent(endpoint.description)

        return fetchResources(url: exchange)
    }
}
