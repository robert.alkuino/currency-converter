//
//  Utils.swift
//  CurrencyConverter
//
//  Created by Robert John Alkuino on 10/7/22.
//

import Foundation
import UIKit

func setGradientBackground(view:UIView) {
    let colorTop =  UIColor(red: 255.0/255.0, green: 149.0/255.0, blue: 0.0/255.0, alpha: 1.0).cgColor
    let colorBottom = UIColor(red: 255.0/255.0, green: 94.0/255.0, blue: 58.0/255.0, alpha: 1.0).cgColor
                
    let gradientLayer = CAGradientLayer()
    gradientLayer.colors = [colorTop, colorBottom]
    gradientLayer.locations = [0.0, 1.0]
    gradientLayer.frame = view.bounds
            
    view.layer.insertSublayer(gradientLayer, at:0)
}

func createShadowAndRadius(view:UIView) {
    // corner radius
    view.layer.cornerRadius = 10

    // shadow
    view.layer.shadowColor = UIColor.black.cgColor
    view.layer.shadowOffset = CGSize(width: 3, height: 3)
    view.layer.shadowOpacity = 0.7
    view.layer.shadowRadius = 2.0
}

class CardsCollectionFlowLayout: UICollectionViewFlowLayout {
    private let itemHeight = 150
    private let itemWidth = 225
    
    // The prepare() methodis called to tell the collection view layout object to update the current layout.
    // Layout updates occur the first time the collection view presents its content and whenever the layout is invalidated.

    override func prepare() {
        guard let collectionView = collectionView else { return }
        
        scrollDirection = .horizontal
        itemSize = CGSize(width: itemWidth, height: itemHeight)
        
        let peekingItemWidth = itemSize.width / 10
        let horizontalInsets = (collectionView.frame.size.width - itemSize.width) / 2
        
        collectionView.contentInset = UIEdgeInsets(top: 0, left: horizontalInsets, bottom: 0, right: horizontalInsets)
        minimumLineSpacing = horizontalInsets - peekingItemWidth
    }
    
}
