//
//  Config.swift
//  CurrencyConverter
//
//  Created by Robert John Alkuino on 10/8/22.
//

import Foundation


let COMMISSION_RATE:Double = 0.07
let INITIAL_USD:Double = 0
let INITIAL_JPY:Double = 0
let INITIAL_EUR:Double = 1000
var INITIAL_FREE_COMMISION:Int = 5

let INITIAL_CURRENCY_FROM = "USD"
let INITIAL_CURRENCY_TO = "EUR"
