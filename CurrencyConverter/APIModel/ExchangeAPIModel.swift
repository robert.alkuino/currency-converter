//
//  ExchangeAPIModel.swift
//  CurrencyConverter
//
//  Created by Robert John Alkuino on 10/7/22.
//

import Foundation

enum Currency: String, CaseIterable {
    case JPY = "JPY", USD = "USD", EUR = "EUR"
}

public struct Exchange: Codable {
  let amount: String
  let currency: String
    
  init(amount: String, currency: String) {
     self.amount = amount
     self.currency = currency
  }
}
