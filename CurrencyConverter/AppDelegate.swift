//
//  AppDelegate.swift
//  CurrencyConverter
//
//  Created by Robert John Alkuino on 10/7/22.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow()
        let controller = UIViewController()
        controller.view.backgroundColor = .white
        window?.rootViewController = controller
        window?.makeKeyAndVisible()
    
        return true
    }


}

