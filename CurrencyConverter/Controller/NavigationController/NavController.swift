//
//  NavController.swift
//  CurrencyConverter
//
//  Created by Robert John Alkuino on 10/7/22.
//

import UIKit

class NavController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let colorTop =  UIColor(red: 255.0/255.0, green: 149.0/255.0, blue: 0.0/255.0, alpha: 1.0)
        
        UINavigationBar.appearance().barTintColor = colorTop
        UINavigationBar.appearance().tintColor = colorTop
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
    }

}
