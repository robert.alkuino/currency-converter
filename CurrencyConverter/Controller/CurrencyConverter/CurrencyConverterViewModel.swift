//
//  CurrencyConverterViewModel.swift
//  CurrencyConverter
//
//  Created by Robert John Alkuino on 10/7/22.
//

import Foundation
import RxSwift
import RxCocoa

class CurrencyConverterViewModel {
    //trigger input
    let fromValue = BehaviorRelay<Double>(value: 0.0)
    let toValue = BehaviorRelay<Double>(value: 0.0)
    let fromUnit = BehaviorRelay<String>(value: INITIAL_CURRENCY_FROM)
    let toUnit = BehaviorRelay<String>(value: INITIAL_CURRENCY_TO)
    
    //trigger output
    let fromValueResult = BehaviorRelay<Double>(value: 0.0)
    let toValueResult = BehaviorRelay<Double>(value: 0.0)
    let myBalances = BehaviorRelay<[Balance]>(value:[])
    
    //textfield binds
    let fromTxtValue = BehaviorRelay<String>(value: "0")
    let toTxtValue = BehaviorRelay<String>(value: "0")
    
    let commissionTxt = BehaviorRelay<String>(value: "")
    
    let isSpinnerShow = BehaviorRelay<Bool>(value: false)
    
    var pickerItems:[String] = []
    
    let disposeBag = DisposeBag()
    
    init(didPressButton: Observable<Void>) {
        
        let initialBalance = [Balance(amount: INITIAL_USD, currency: Currency.USD.rawValue),
                       Balance(amount: INITIAL_JPY, currency: Currency.JPY.rawValue),
                       Balance(amount: INITIAL_EUR, currency: Currency.EUR.rawValue)]
        myBalances.accept(initialBalance)
        
        
        Currency.allCases.forEach {
            pickerItems.append($0.rawValue)
        }
        
        fromValue
            .asObservable()
            .skip(1)
            .subscribe(on: MainScheduler.instance)
            .subscribe(
                onNext: {  value in
                    self.getCurrentConvertion(value: value,
                                                      changedInFrom: true)
            }).disposed(by: disposeBag)
        
        toValue
            .asObservable()
            .skip(1)
            .subscribe(on: MainScheduler.instance)
            .subscribe(
                onNext: {  value in
                    self.getCurrentConvertion(value: value,
                                                      changedInFrom: false)
            }).disposed(by: disposeBag)
        
        fromUnit
            .asObservable()
            .skip(1)
            .subscribe(on: MainScheduler.instance)
            .subscribe(
                onNext: { value in
                    if let dValue = Double(self.fromTxtValue.value) {
                        self.getCurrentConvertion(value: dValue,
                                                          changedInFrom: true)
                    }
                    
            }).disposed(by: disposeBag)
        
        toUnit
            .asObservable()
            .skip(1)
            .subscribe(on: MainScheduler.instance)
            .subscribe(
                onNext: { value in
                    if let dValue = Double(self.toTxtValue.value) {
                        self.getCurrentConvertion(value: dValue,
                                                          changedInFrom: false)
                    }
            }).disposed(by: disposeBag)

        didPressButton
            .subscribe(
                onNext: { _ in
                    self.convert()
            }).disposed(by: disposeBag)
    }
    
    
    func getCurrentConvertion(value:Double,changedInFrom:Bool){
        isSpinnerShow.accept(true)
        let _fromVal = changedInFrom ? fromUnit.value:toUnit.value
        let _toVal = changedInFrom ? toUnit.value:fromUnit.value
        APIService<Exchange>
            .latestExchangeConvertion(from: .exchange(value: "\(value)",
                                                      from: _fromVal,
                                                      to: _toVal))
            .subscribe(
                onNext: { result in
                    print(result)
                    if let amount = Double(result.amount) {
                        DispatchQueue.main.async { [self] in
                            if changedInFrom {
                                toValueResult.accept(amount)
                            } else {
                                fromValueResult.accept(amount)
                            }
                        }
                    }
                },
                onError: { error in
                   print(error.localizedDescription)
                },
                onCompleted: {
                    self.isSpinnerShow.accept(false)
                    print("Completed event.")
                }).disposed(by: disposeBag)
    }
    
    func convert() {
        if let _fromValue = Double(fromTxtValue.value) , let _toValue = Double(toTxtValue.value) {
            var _balances = self.myBalances.value
            
            if let row = _balances.firstIndex(where: {$0.currency == fromUnit.value}) {
                let commission = (_fromValue * COMMISSION_RATE)
                let amtPlusComm = INITIAL_FREE_COMMISION != 0 ? commission+_fromValue:0
                _balances[row].lessAmount(val: amtPlusComm)
                if _balances[row].amount > 0 {
                    if let row2 = _balances.firstIndex(where: {$0.currency == toUnit.value}) {
                        _balances[row2].addAmount(val: _toValue)
                    }
                    myBalances.accept(_balances)
                    let youHaveStr = "You have converted \(_fromValue) \(fromUnit.value) to \(_toValue) \(toUnit.value)"
                    commissionTxt.accept("CONVERTED SUCCESFULLY! \n \(youHaveStr) \n Commission Fee - \(String(format: "%.2f", commission)) \(fromUnit.value)(LESS)")
                    INITIAL_FREE_COMMISION -= 1
                }
            }
            
            
        }
    }
}
