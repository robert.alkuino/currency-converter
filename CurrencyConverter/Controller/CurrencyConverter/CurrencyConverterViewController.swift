//
//  CurrencyConverterViewController.swift
//  CurrencyConverter
//
//  Created by Robert John Alkuino on 10/7/22.
//

import UIKit
import RxSwift
import AYPopupPickerView

class CurrencyConverterViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var balanceCollectionView: UICollectionView!
    private var currentSelectedIndex = 0
    
    @IBOutlet weak var outsideFormView: UIView!
    @IBOutlet weak var toImage: UIImageView!
    @IBOutlet weak var fromImage: UIImageView!
    @IBOutlet weak var currencyFormView: UIView!
    @IBOutlet weak var exchangeButton: UIButton!
    @IBOutlet weak var fromCurrencyButton: UIButton!
    @IBOutlet weak var toCurrencyButton: UIButton!
    @IBOutlet weak var toTextField: UITextField!
    @IBOutlet weak var fromTextField: UITextField!
    private var viewModel:CurrencyConverterViewModel?
    
    public lazy var spinnerView : PSOverlaySpinner = {
        let loadingView : PSOverlaySpinner = PSOverlaySpinner()
        return loadingView
    }()
    
    @IBOutlet weak var commissionLabel: UILabel!
    let popupPickerView = AYPopupPickerView()
    
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        balanceCollectionView.register(UINib(nibName:"CardCollectionViewCell", bundle: nil),
                                       forCellWithReuseIdentifier: "CardCollectionViewCell")
        balanceCollectionView.collectionViewLayout = CardsCollectionFlowLayout()
        
        createShadowAndRadius(view: currencyFormView)
        createShadowAndRadius(view: exchangeButton)
        createShadowAndRadius(view: outsideFormView)
        setGradientBackground(view: exchangeButton)
        
        commissionLabel.lineBreakMode = .byWordWrapping
        commissionLabel.numberOfLines = 0
        
        self.view.addSubview(spinnerView)
        
        spinnerView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        spinnerView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        spinnerView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        spinnerView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        
        
        fromTextField.delegate = self
        toTextField.delegate = self
        
        let tap = UITapGestureRecognizer(target: self,
                                         action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        bind()
    }
    
    @IBAction func didSelectToCurrency(_ sender: UIButton) {
        
        guard let viewModel = self.viewModel else { return }
        
        popupPickerView.display(itemTitles: viewModel.pickerItems, doneHandler: {
            let selectedIndex = self.popupPickerView.pickerView.selectedRow(inComponent: 0)
            
            let selectedStr = viewModel.pickerItems[selectedIndex]
            
            sender.setTitle(selectedStr, for: .normal)
            
            if sender == self.fromCurrencyButton {
                self.viewModel?.fromUnit.accept(selectedStr)
                self.fromImage.image = UIImage(named:"\(selectedStr)")
            } else {
                self.viewModel?.toUnit.accept(selectedStr)
                self.toImage.image = UIImage(named:"\(selectedStr)")
            }
        })
    }
    
    
    func bind() {
        viewModel = CurrencyConverterViewModel(didPressButton: exchangeButton.rx.tap.asObservable())
        
        guard let viewModel = self.viewModel else { return }
        
        fromTextField.rx.editingDidEnd.subscribe(onNext: { value in
            viewModel.fromValue.accept(value)
        }).disposed(by: disposeBag)
            
        toTextField.rx.editingDidEnd.subscribe(onNext: { value in
            viewModel.toValue.accept(value)
        }).disposed(by: disposeBag)
        
        viewModel.fromValueResult.subscribe(onNext: { value in
            self.fromTextField.text = "\(value)"
            viewModel.fromTxtValue.accept("\(value)")
        }).disposed(by: disposeBag)
        
        viewModel.toValueResult
            .subscribe(onNext: { value in
            self.toTextField.text = "\(value)"
            viewModel.toTxtValue.accept("\(value)")
        }).disposed(by: disposeBag)
                                               
        fromTextField.rx.text
           .orEmpty
           .bind(to: viewModel.fromTxtValue)
           .disposed(by: disposeBag)

        toTextField.rx.text
           .orEmpty
           .bind(to: viewModel.toTxtValue)
           .disposed(by: disposeBag)
        
        viewModel.myBalances.subscribe(onNext: { value in
            self.balanceCollectionView.reloadData()
            print("collectionview reloaded")
        }).disposed(by: disposeBag)
        
        viewModel.isSpinnerShow.subscribe(onNext: { value in
            value ? self.spinnerView.show():self.spinnerView.hide()
        }).disposed(by: disposeBag)
        
        viewModel.commissionTxt.subscribe(onNext: { value in
            self.commissionLabel.text = value
        }).disposed(by: disposeBag)
                                               
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        let currentCell = balanceCollectionView.cellForItem(at: IndexPath(row: Int(currentSelectedIndex), section: 0))
        currentCell?.transformToStandard()
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView,
                                   withVelocity velocity: CGPoint,
                                   targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        guard scrollView == balanceCollectionView else {
            return
        }
        
        targetContentOffset.pointee = scrollView.contentOffset
        
        let flowLayout = balanceCollectionView.collectionViewLayout as! CardsCollectionFlowLayout
        let cellWidthIncludingSpacing = flowLayout.itemSize.width + flowLayout.minimumLineSpacing
        let offset = targetContentOffset.pointee
        let horizontalVelocity = velocity.x
        
        var selectedIndex = currentSelectedIndex
        
        switch horizontalVelocity {
        // On swiping
        case _ where horizontalVelocity > 0 :
            selectedIndex = currentSelectedIndex + 1
        case _ where horizontalVelocity < 0:
            selectedIndex = currentSelectedIndex - 1
            
        // On dragging
        case _ where horizontalVelocity == 0:
            let index = (offset.x + scrollView.contentInset.left) / cellWidthIncludingSpacing
            let roundedIndex = round(index)
            
            selectedIndex = Int(roundedIndex)
        default:
            print("Incorrect velocity for collection view")
        }
        guard let viewModel = self.viewModel else { return }
        
        let safeIndex = max(0, min(selectedIndex, viewModel.myBalances.value.count - 1))
        let selectedIndexPath = IndexPath(row: safeIndex, section: 0)
        
        flowLayout.collectionView!.scrollToItem(at: selectedIndexPath, at: .centeredHorizontally, animated: true)
        
        let previousSelectedIndex = IndexPath(row: Int(currentSelectedIndex), section: 0)
        let previousSelectedCell = balanceCollectionView.cellForItem(at: previousSelectedIndex)
        let nextSelectedCell = balanceCollectionView.cellForItem(at: selectedIndexPath)
        
        currentSelectedIndex = selectedIndexPath.row
        
        previousSelectedCell?.transformToStandard()
        nextSelectedCell?.transformToLarge()
    }
}

extension CurrencyConverterViewController : UICollectionViewDelegate, UICollectionViewDataSource{

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (viewModel != nil) ? viewModel!.myBalances.value.count:0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CardCollectionViewCell",
                                                      for: indexPath) as! CardCollectionViewCell
        
        if let viewModel = self.viewModel {
            let data = viewModel.myBalances.value[indexPath.row]
            cell.balance = data
        }
        
        if currentSelectedIndex == indexPath.row {
            cell.transformToLarge()
        }
        
        return cell
    }
}


