//
//  Balance.swift
//  CurrencyConverter
//
//  Created by Robert John Alkuino on 10/8/22.
//

import Foundation

public struct Balance: Codable {
    var amount: Double
    let currency: String

    init(amount: Double, currency: String) {
     self.amount = amount
     self.currency = currency
    }
    
    mutating func addAmount(val:Double) {
        self.amount += val
    }
    
    mutating func lessAmount(val:Double) {
        self.amount -= val
    }

}
